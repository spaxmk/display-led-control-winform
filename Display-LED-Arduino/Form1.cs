﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Display_LED_Arduino
{
    public partial class Form1 : Form
    {
        bool isConnected = false;
        String[] ports;
        SerialPort port;
        
        public Form1()
        {
            InitializeComponent();
            DisableControls();
            getAvailableComPorts();

            foreach (string port in ports) 
            {
                cbCOM.Items.Add(port);
                if(ports[0] != null)
                {
                    cbCOM.SelectedItem = ports[0];
                }
            }
        }

        private void btnConn_Click(object sender, EventArgs e)
        {
            if (!isConnected)
            {
                ConnectToArduino();
            }
            else
            {
                DisconnectFromArduino();
            }
        }

        void getAvailableComPorts()
        {
            ports = SerialPort.GetPortNames();
        }

        private void ConnectToArduino()
        {
            string selectedPort = cbCOM.GetItemText(cbCOM.SelectedItem);
            port = new SerialPort(selectedPort, 9600, Parity.None, 8, StopBits.One);
            try
            {
                port.Open();
                port.Write("#STAR\n");
                btnConn.Text = "Disconnect";
                isConnected = true;
                EnableControls();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Port COM3 busy");
                isConnected = false;
            }
        }

        private void EnableControls()
        {
            rtbDisplayText.Enabled = true;
            btnSend.Enabled = true;
            cbLed1.Enabled = true;
            cbLed2.Enabled = true;
            cbLed3.Enabled = true;

        }

        private void DisableControls()
        {
            rtbDisplayText.Enabled = false;
            btnSend.Enabled = false;
            cbLed1.Enabled = false;
            cbLed2.Enabled = false;
            cbLed3.Enabled = false;
        }

        private void cbLed1_CheckedChanged(object sender, EventArgs e)
        {
            if (isConnected)
            {
                if (cbLed1.Checked)
                {
                    port.Write("#LED1ON\n");
                }
                else
                {
                    port.Write("#LED1OF\n");
                }
            }
        }

        private void cbLed2_CheckedChanged(object sender, EventArgs e)
        {
            if (isConnected)
            {
                if (cbLed1.Checked)
                {
                    port.Write("#LED2ON\n");
                }
                else
                {
                    port.Write("#LED2OF\n");
                }
            }
        }

        private void cbLed3_CheckedChanged(object sender, EventArgs e)
        {
            if (isConnected)
            {
                if (cbLed1.Checked)
                {
                    port.Write("#LED2ON\n");
                }
                else
                {
                    port.Write("#LED2OF\n");
                }
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (isConnected)
            {
                port.Write("#TEXT" + rtbDisplayText.Text + "#\n");
            }
        }

        private void DisconnectFromArduino()
        {
            isConnected = false;
            port.Write("#STOP\n");
            port.Close();
            btnConn.Text = "Connect";
            DisableControls();
        }

        
    }
}

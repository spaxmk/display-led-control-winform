﻿
namespace Display_LED_Arduino
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbLCDControl = new System.Windows.Forms.GroupBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.rtbDisplayText = new System.Windows.Forms.RichTextBox();
            this.gbLedControl = new System.Windows.Forms.GroupBox();
            this.cbLed3 = new System.Windows.Forms.CheckBox();
            this.cbLed2 = new System.Windows.Forms.CheckBox();
            this.cbLed1 = new System.Windows.Forms.CheckBox();
            this.gbSerialConn = new System.Windows.Forms.GroupBox();
            this.cbCOM = new System.Windows.Forms.ComboBox();
            this.btnConn = new System.Windows.Forms.Button();
            this.gbLCDControl.SuspendLayout();
            this.gbLedControl.SuspendLayout();
            this.gbSerialConn.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbLCDControl
            // 
            this.gbLCDControl.Controls.Add(this.btnSend);
            this.gbLCDControl.Controls.Add(this.rtbDisplayText);
            this.gbLCDControl.Location = new System.Drawing.Point(12, 12);
            this.gbLCDControl.Name = "gbLCDControl";
            this.gbLCDControl.Size = new System.Drawing.Size(206, 146);
            this.gbLCDControl.TabIndex = 0;
            this.gbLCDControl.TabStop = false;
            this.gbLCDControl.Text = "LCD Control";
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.Location = new System.Drawing.Point(54, 93);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(99, 37);
            this.btnSend.TabIndex = 1;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // rtbDisplayText
            // 
            this.rtbDisplayText.Location = new System.Drawing.Point(6, 22);
            this.rtbDisplayText.Name = "rtbDisplayText";
            this.rtbDisplayText.Size = new System.Drawing.Size(194, 56);
            this.rtbDisplayText.TabIndex = 0;
            this.rtbDisplayText.Text = "";
            // 
            // gbLedControl
            // 
            this.gbLedControl.Controls.Add(this.cbLed3);
            this.gbLedControl.Controls.Add(this.cbLed2);
            this.gbLedControl.Controls.Add(this.cbLed1);
            this.gbLedControl.Location = new System.Drawing.Point(258, 12);
            this.gbLedControl.Name = "gbLedControl";
            this.gbLedControl.Size = new System.Drawing.Size(200, 78);
            this.gbLedControl.TabIndex = 1;
            this.gbLedControl.TabStop = false;
            this.gbLedControl.Text = "LED Control";
            // 
            // cbLed3
            // 
            this.cbLed3.AutoSize = true;
            this.cbLed3.Location = new System.Drawing.Point(128, 36);
            this.cbLed3.Name = "cbLed3";
            this.cbLed3.Size = new System.Drawing.Size(55, 19);
            this.cbLed3.TabIndex = 2;
            this.cbLed3.Text = "LED 3";
            this.cbLed3.UseVisualStyleBackColor = true;
            this.cbLed3.CheckedChanged += new System.EventHandler(this.cbLed3_CheckedChanged);
            // 
            // cbLed2
            // 
            this.cbLed2.AutoSize = true;
            this.cbLed2.Location = new System.Drawing.Point(67, 36);
            this.cbLed2.Name = "cbLed2";
            this.cbLed2.Size = new System.Drawing.Size(55, 19);
            this.cbLed2.TabIndex = 1;
            this.cbLed2.Text = "LED 2";
            this.cbLed2.UseVisualStyleBackColor = true;
            this.cbLed2.CheckedChanged += new System.EventHandler(this.cbLed2_CheckedChanged);
            // 
            // cbLed1
            // 
            this.cbLed1.AutoSize = true;
            this.cbLed1.Location = new System.Drawing.Point(6, 36);
            this.cbLed1.Name = "cbLed1";
            this.cbLed1.Size = new System.Drawing.Size(55, 19);
            this.cbLed1.TabIndex = 0;
            this.cbLed1.Text = "LED 1";
            this.cbLed1.UseVisualStyleBackColor = true;
            this.cbLed1.CheckedChanged += new System.EventHandler(this.cbLed1_CheckedChanged);
            // 
            // gbSerialConn
            // 
            this.gbSerialConn.Controls.Add(this.cbCOM);
            this.gbSerialConn.Controls.Add(this.btnConn);
            this.gbSerialConn.Location = new System.Drawing.Point(12, 175);
            this.gbSerialConn.Name = "gbSerialConn";
            this.gbSerialConn.Size = new System.Drawing.Size(261, 100);
            this.gbSerialConn.TabIndex = 2;
            this.gbSerialConn.TabStop = false;
            this.gbSerialConn.Text = "Serial Connection";
            // 
            // cbCOM
            // 
            this.cbCOM.FormattingEnabled = true;
            this.cbCOM.Location = new System.Drawing.Point(111, 36);
            this.cbCOM.Name = "cbCOM";
            this.cbCOM.Size = new System.Drawing.Size(121, 23);
            this.cbCOM.TabIndex = 1;
            // 
            // btnConn
            // 
            this.btnConn.Location = new System.Drawing.Point(19, 36);
            this.btnConn.Name = "btnConn";
            this.btnConn.Size = new System.Drawing.Size(75, 23);
            this.btnConn.TabIndex = 0;
            this.btnConn.Text = "Connect";
            this.btnConn.UseVisualStyleBackColor = true;
            this.btnConn.Click += new System.EventHandler(this.btnConn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 292);
            this.Controls.Add(this.gbSerialConn);
            this.Controls.Add(this.gbLedControl);
            this.Controls.Add(this.gbLCDControl);
            this.Name = "Form1";
            this.Text = "Arduino connect";
            this.gbLCDControl.ResumeLayout(false);
            this.gbLedControl.ResumeLayout(false);
            this.gbLedControl.PerformLayout();
            this.gbSerialConn.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbLCDControl;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.RichTextBox rtbDisplayText;
        private System.Windows.Forms.GroupBox gbLedControl;
        private System.Windows.Forms.CheckBox cbLed3;
        private System.Windows.Forms.CheckBox cbLed2;
        private System.Windows.Forms.CheckBox cbLed1;
        private System.Windows.Forms.GroupBox gbSerialConn;
        private System.Windows.Forms.ComboBox cbCOM;
        private System.Windows.Forms.Button btnConn;
    }
}

